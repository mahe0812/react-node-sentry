import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import dotenv from 'dotenv';
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";

Sentry.init({
    dsn: "https://03dbeac08a714d6f8d9d7dcbd3690a6c@o560509.ingest.sentry.io/5696121",
    integrations: [new Integrations.BrowserTracing()],
  
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });

  
dotenv.config();
console.log(process.env);

ReactDOM.render( < App / > , document.getElementById('root'));
registerServiceWorker();